
// Create an app
var server = require('diet')
var app = server()
app.listen(process.env.PORT || 8080)

var herokuApp = process.env.HEROKU_APP_NAME
if (herokuApp) {
  app.host(herokuApp + '.herokuapp.com')
}

// When http://localhost:8000/ is requested, respond with "Hello World!"
app.get('/', function ($) {
  $.end('Hello World!')
})
